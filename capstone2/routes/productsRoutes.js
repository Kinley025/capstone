const express = require("express");
const productControllers = require("../controllers/productsControllers");

const auth = require("../auth.js");

const router = express.Router();

// without params

router.post("/addProducts", auth.verify, productControllers.addProducts);

router.get("/", auth.verify, productControllers.allProducts);

router.get("/activeProducts", productControllers.activeProducts);



// with params

router.delete("/:id/delete", auth.verify, productControllers.deleteProduct);

router.get("/allProducts/:id", auth.verify, productControllers.specificAllProduct);

router.patch("/allProducts/:id/update", auth.verify, productControllers.updateProduct)

router.get("/:id", productControllers.specificProduct);

router.patch("/allProducts/:id/updateStatus", auth.verify, productControllers.updateStatus);



module.exports = router;