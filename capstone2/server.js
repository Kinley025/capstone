const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/usersRoutes.js");
const productRoutes = require("./routes/productsRoutes.js");
const orderRoutes = require("./routes/ordersRoutes.js");

const port = 4001;


const app = express();

mongoose.connect("mongodb+srv://admin:admin@batch288bendecio.3fb8drl.mongodb.net/EcommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

const db = mongoose.connection;

db.on("error", console.error.bind("Error! Cannot connect to the cloud database!"));
db.once("open", () => console.log("Connected to the cloud database!"));



app.use(express.json());
app.use(cors());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);




app.listen(port, () => console.log(`Server is now connected to port: ${port}!`));