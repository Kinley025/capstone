const mongoose = require("mongoose");

const productSchema = mongoose.Schema({

	name: {
		type: String,
		required: [true, "Please input product name!"]
	},

	description: {
		type: String,
		required: [true, "Please input description!"]
	},

	price: {
		type: Number,
		required: [true, "Please input price!"]
	},

	qty: {
		type: Number,
		default: 1
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	}
})


const Products = mongoose.model("Products", productSchema);

module.exports = Products;