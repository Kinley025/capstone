const mongoose = require("mongoose")

const orderSchema = mongoose.Schema({


	userId: {
		type: String,
		required: true
	},

	product: [{

		productId: {
			type: String,
			required: true
		},

		productName: {
			type: String,
			required: true
		},

		qty: {
			type: Number,
			default: 1
		}

	}],

	total: {
		type: Number,
		required: true
	},

	purchaseOn: {
		type: Date,
		default: new Date()
	}

})


const Orders = mongoose.model("Orders", orderSchema);

module.exports = Orders;
