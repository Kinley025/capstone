import {Button, Container, Form, Nav, Navbar} from 'react-bootstrap'
import Image from '../images/Logo.png'
import UserContext from '../UserContext.js'
import {useContext, useState} from 'react';
import {NavLink, Link} from 'react-router-dom'

export default function NavBar(){

	const {user} = useContext(UserContext)


	return(

		<>
			<Navbar className = "nav mt-2 mb-2">
			      <Container fluid>
			      		<Navbar.Brand href = "#top"><img src = {Image} className = "img-logo pt-2"/></Navbar.Brand>
			      	  	<Nav>
			           		<Nav.Link as = {NavLink} to = "/" className = "text-light nav-hover px-4">Home</Nav.Link>
			           		<Nav.Link as = {NavLink} to = "/shop" className = "text-light nav-hover px-4">Shop</Nav.Link>
			          
			            	{
			            		localStorage.getItem('token') === null ?
			            		<>
			            			<Nav.Link as = {NavLink} to = "/register" className = "text-light nav-hover px-4">Register</Nav.Link>
			            			<Nav.Link as = {NavLink} to = "/login" className = "text-light nav-hover px-4">Login</Nav.Link>

			        	        </> :
			        	        <>	
			        	        	{
			        	        		user.isAdmin ? 

			        	        	<Nav.Link as = {NavLink} to = "/dashboard" className = "text-light nav-hover px-4">Dashboard</Nav.Link>
			        	        	
			        	        	: null
			        	        	
			        	        	}
			        	        	<Nav.Link as = {NavLink} to = "/logout"className = "text-light nav-hover px-4">Logout</Nav.Link>
			        	       	</>
			            	}

			          	</Nav>
			      </Container>
			    </Navbar>
		</>

		)
}