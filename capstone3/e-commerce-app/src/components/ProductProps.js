import {Button, Card, Col, Container, Row} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, useParams} from 'react-router-dom';
import Swal2 from 'sweetalert2';



export default function ProductProps({onActivate,onDeactivate, ...props}){

	const {_id, name, description, qty, isActive, price} = props.productProp;

	const deleteProduct = () => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/delete`, {
			method: "DELETE",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(result => result.json())
		.then(data => {
			
			if(data){

				Swal2.fire({
					title: "Deleted successfully",
					icon: "success",
					text: `Product has been delete to the database`
				})
			} else {

				Swal2.fire({
					title: "Something went wrong",
					icon: "error",
					text: `Please try again`
				})
			}
		})
	}

	return( 
		<Container className = "mt-2 props-container bg-light p-2 rounded-3"> 
			<Row>
				<Col className = "d-flex align-items-center justify-content-center"> 
					<h5 className = "col-10 mx-auto">{name}</h5>
				</Col>

				<Col className = "d-flex align-items-center justify-content-center"> 
					<p>{description.substring(0, 50)} ...</p>
				</Col>

				<Col className = "d-flex align-items-center justify-content-center"> 
					<h5>{qty.toLocaleString()}</h5>
				</Col>

				<Col className = "d-flex align-items-center justify-content-center"> 
					<h5>{price.toLocaleString()}</h5>
				</Col>

				<Col className = "d-flex align-items-center justify-content-center"> 
					{
						isActive ? 
						<h5>Active</h5>
						:
						<h5>Inactive</h5>
					}
				</Col>

				<Col>
					<Button className = "btn-update mb-1" as = {Link} to = {`/products/update/${_id}`} >Update</Button>
					{
						isActive ? 
						<Button className = "btn-activate mb-1" 
				    	 	onClick = {onDeactivate}
							>Deactivate</Button>
						:
						<Button className = "btn-activate mb-1" 
					    	 	onClick = {onActivate}
								>Activate</Button>
					}
					<Button className = "btn-delete" onClick = {deleteProduct} >Delete</Button>
				</Col>

			</Row>
		</Container>
		)
}