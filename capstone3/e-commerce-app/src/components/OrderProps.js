import {Button, Card, Col, Container, Row} from 'react-bootstrap';
import {Link, useParams} from 'react-router-dom'
import {useContext} from 'react';
import UserContext from '../UserContext.js';



export default function OrderProps(props){

	const {userId, product, total} = props.orderProp;



	const {user} = useContext(UserContext);

return(

			<Col className = "col-4 mx-auto mb-5">
				<Card className = "d-flex align-items-start">
					<Card.Body>
												{
							product.map(products => (
								<>
									<Card.Subtitle>Product Id:</Card.Subtitle>
									<Card.Text>{products.productId}</Card.Text>
							        <Card.Subtitle>Product Name:</Card.Subtitle>
							        <Card.Text>{products.productName}</Card.Text>
							        <Card.Subtitle>Quantity:</Card.Subtitle>
							        <Card.Text>{products.qty}</Card.Text>
								</>
							))
						}

						<Card.Subtitle>Total:</Card.Subtitle>
						<Card.Text>PHP {total.toLocaleString()}</Card.Text>
					</Card.Body>
				</Card>
			</Col>

	)

}