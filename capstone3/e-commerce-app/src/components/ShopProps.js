import {Button, Card, Col, Container, Row} from 'react-bootstrap';
import {Link, useParams} from 'react-router-dom'
import {useContext} from 'react';
import UserContext from '../UserContext.js';



export default function ShopProps(props){

	const {_id, name, description, qty, price} = props.shopProp;

	const {user} = useContext(UserContext);

return(


			<Col className = "col-4 mx-auto mb-5">
				<Card className = "d-flex align-items-start">
					<Card.Body>
						<Card.Title>{name}
						</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text className = "mb-4">{description.substring(0, 100)}</Card.Text>
				        <Card.Subtitle>Stocks:</Card.Subtitle>
				        <Card.Text className = "mb-4">{qty}</Card.Text>
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text className = "mb-4">PHP {price.toLocaleString()}</Card.Text>
					    {
					   	user.id !== null ?
				     	<Button variant="primary" as = {Link} to = {`/orders/${_id}/`}>Check Out</Button>
				      	:
				      	<Button as = {Link} to = "/login">Login in to Buy</Button>
					    }
					</Card.Body>
				</Card>
			</Col>

	)

}