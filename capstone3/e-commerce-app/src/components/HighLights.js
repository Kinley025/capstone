import {Container, Row, Col, Carousel} from 'react-bootstrap';
import Image1 from '../images/highlights1.jpg';
import Image2 from '../images/highlights2.jpg';
import Image3 from '../images/highlights4.jpg';

export default function HighLight(){

return(

	<Container className = "Container">
		<Row>
			<Col>
				<Carousel className = "mx-auto mt-3 Carousel">
				      <Carousel.Item>
				        <img
				          className="d-block w-100"
				          src = {Image1}
				          alt = "First slide"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				        <img
				          className="d-block w-100"
				          src = {Image2}
				          alt = "Second slide"
				        />

				      </Carousel.Item>

				      <Carousel.Item>
				        <img
				          className="d-block w-100"
				          src = {Image3}
				          alt = "Third slide"
				        />
				      </Carousel.Item>
				</Carousel>
			</Col>
		</Row>
	</Container>

	)
}