import {Link} from 'react-router-dom';
import {Container} from 'react-bootstrap'
import Image from '../images/error.png'


export default function PageNotFound(){


	return(
		<div className = "d-flex align-items-center justify-content-center flex-column">
			<img src = {Image} id = "error"/>
			<h1 className = "fw-bolder text-light">Page Not Found</h1>
			<p className = "text-light">Go back to the <Link to = "/">homepage.</Link></p>
		</div>
		)
}