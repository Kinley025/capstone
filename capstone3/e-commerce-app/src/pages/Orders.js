import {Container, Col, Row} from 'react-bootstrap';
import OrderProps from '../components/OrderProps.js';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';


export default function Orders(){

	const [order, setOrder] = useState([])


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/allOrders`, {
			method: "GET",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setOrder(data.map(order => {
				return (
					<OrderProps key = {order._id} orderProp = {order} />
				)
			}))
		})
	}, [])

	return(

		<Container fluid="md">
			<h1 className = "text-center text-light fonts mt-3 mb-3">All Orders</h1>
			<Row className = "d-flex align-items-center justify-content-center">
				{order}
			</Row>
		</Container>

		)
}