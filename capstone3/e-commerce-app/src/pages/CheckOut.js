import {useState, useEffect} from 'react';
import {useParams, useNavigate} from 'react-router-dom'
import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import Swal2 from 'sweetalert2';
import { faPlus, faMinus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function CheckOut(){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [qty, setQty] = useState(1);
	const [price, setPrice] = useState('');
	const [total, setTotal] = useState(price);

	const navigate = useNavigate();

	const { id } = useParams();

	const qtyMinus = (e) => {

			setQty(qty - 1)
			setTotal((qty - 1) * price)

	}

	const qtyPlus = (e) => {

		setQty(qty + 1)
		setTotal((qty + 1) * price)
	}

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then(result => result.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
			setTotal(data.price)
		})
	}, [])

	const checkOut = () => {

		fetch(`${process.env.REACT_APP_API_URL}/orders/${id}`, {
			method: "POST",
			headers: {
				"Authorization" : `Bearer ${localStorage.getItem("token")}`,
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				product: {
					qty: qty
				}
			})
		})
		.then(result => result.json())
		.then(data => {
			
			if(data){

				Swal2.fire({
					title: "Order successful",
					icon: "success",
					text: `Thank you for buying our product`
				})

				navigate("/shop")

			} else {

				Swal2.fire({
					title: "Something went wrong",
					icon: "error",
					text: `Please try again`
				})
			}

		})
	}



return(

	<Container>
		<Row>
			<Col>
				<Card className = "card-view">
				   	<Card.Body>
				       	<Card.Title>{name}</Card.Title>
				     	<Card.Subtitle>Description:</Card.Subtitle>
				     	<Card.Text>{description}</Card.Text>
				     	<Card.Subtitle>Price:</Card.Subtitle>
				     	<Card.Text>PHP {price.toLocaleString()}</Card.Text>
				     	<Card.Subtitle>Quantity</Card.Subtitle>
				     	<Card.Text><FontAwesomeIcon className = "me-2 disabled" onClick = {qty === 0 ? null : e => qtyMinus(e)} icon={faMinus} />
				     	{qty}
				     	<FontAwesomeIcon onClick = {e => qtyPlus(e)} className = "ms-2" icon={faPlus}/></Card.Text>
				     	<Card.Subtitle>Total:</Card.Subtitle>
				     	<Card.Text>PHP {total.toLocaleString()}</Card.Text>
				       	<Button variant="primary" onClick = {() => checkOut(id)} >Buy</Button>
				    </Card.Body>
				</Card>
			</Col>
		</Row>
	</Container>

	)
}